Source: openxr-sdk-source
Maintainer: Ryan Pavlik <ryan@ryanpavlik.com>
Uploaders: Andrew Lee (李健秋) <ajqlee@debian.org>
Section: libs
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 12),
               libgl1-mesa-dev,
               libglvnd-dev,
               libjsoncpp-dev,
               libopengl0,
               libvulkan-dev,
               libx11-dev,
               libx11-xcb-dev,
               libxcb-dri2-0-dev,
               libxcb-glx0-dev,
               libxcb-icccm4-dev,
               libxcb-keysyms1-dev,
               libxcb-randr0-dev,
               libxrandr-dev,
               libxxf86vm-dev,
               markdown,
               mesa-common-dev,
               python3-jinja2
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/openxr-team/openxr-sdk-source
Vcs-Git: https://salsa.debian.org/openxr-team/openxr-sdk-source.git
Homepage: https://github.com/KhronosGroup/OpenXR-SDK-Source
Rules-Requires-Root: no

Package: libopenxr-utils
Architecture: any
Section: graphics
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: OpenXR software development kit -- utilities
 OpenXR is an API specification for writing portable, cross-platform
 virtual reality (VR) and augmented reality (AR) software.
 .
 This package includes two utility binaries from the
 OpenXR-SDK-Source repository:
 .
   - openxr_runtime_list - Provides information on the active
     OpenXR runtime.
   - hello_xr - A sample OpenXR application that works with
     multiple graphics APIs.

Package: libopenxr-loader1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: OpenXR loader library
 OpenXR is an API specification for writing portable, cross-platform
 virtual reality (VR) and augmented reality (AR) software.
 .
 This package contains the OpenXR loader, which is used by
 OpenXR-based software to access the active runtime and
 handle API layers.
 .
 To run OpenXR-based software, an OpenXR runtime is needed
 in addition to this package.

Package: libopenxr-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libopenxr-loader1 (= ${binary:Version}),
         ${misc:Depends}
Suggests: openxr-layer-corevalidation (= ${binary:Version})
Description: OpenXR software development kit -- development headers
 OpenXR is an API specification for writing portable, cross-platform
 virtual reality (VR) and augmented reality (AR) software.
 .
 This package includes header files needed for development.

Package: openxr-layer-corevalidation
Architecture: any
Multi-Arch: same
Section: graphics
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: OpenXR software development kit -- validation layer
 OpenXR is an API specification for writing portable, cross-platform
 virtual reality (VR) and augmented reality (AR) software.
 .
 This package contains the XR_APILAYER_LUNARG_core_validation
 OpenXR API layer from the OpenXR-SDK-Source repository. It may be
 explicitly enabled by an environment variable or an application.
 .
 The XR_APILAYER_LUNARG_core_validation layer validates API call
 input and can provide a summary of usage errors.

Package: openxr-layer-apidump
Architecture: any
Multi-Arch: same
Section: graphics
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: OpenXR software development kit -- API dump layer
 OpenXR is an API specification for writing portable, cross-platform
 virtual reality (VR) and augmented reality (AR) software.
 .
 This package contains the XR_APILAYER_LUNARG_api_dump
 OpenXR API layer from the OpenXR-SDK-Source repository. It may be
 explicitly enabled by an environment variable or an application.
 .
 The XR_APILAYER_LUNARG_api_dump layer outputs or saves a dump of all OpenXR
 calls made.
